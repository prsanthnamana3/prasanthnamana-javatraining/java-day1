import sun.reflect.generics.tree.Tree;

import java.util.*;

    /*
    *
    *
    *
    *
    * */
public class CollectionExercise {
    public static void main(String[] args){
        CollectionExercise ce  = new CollectionExercise();
        int[] arr = {1,2,3,4,5,6};
       // System.out.println(ce.searchElementInArray(Arrays.asList(arr),3));
        ce.updateElementInArray(2,arr,444);

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(333);
        list.add(4);
        ce.searchElementInArray(list,3);

        ce.copyArrayList(list,new ArrayList<>(list.size()));

        LinkedList linkedList = new LinkedList<>();
        linkedList.add(11);
        linkedList.add(22);
        linkedList.add(33);
        linkedList.add(44);
        ce.linkedListReverse(linkedList);

        ce.insertElementsInLinkedList(linkedList,list);

        ce.shuffleElementsInLinkedList(linkedList);

        HashSet<Integer> hashSet = new HashSet<>(list);
        TreeSet<Integer> treeSet = ce.hashSetToTreeSet(hashSet);

        ce.compareTreeSets();

        ce.getElementLTGiven(treeSet, 2);

        ce.pQToArray(new PriorityQueue<>(list));

        TreeMap<Integer,String> treeMap = new TreeMap<>();
        treeMap.put(1,"Hi");
        treeMap.put(5,"treeMap");
        treeMap.put(2,"this");
        treeMap.put(3,"is a");
        ce.checkIFMapIsEmpty(treeMap);

        ce.getPortionOfMap(treeMap,2);
        ce.getPortionOfMapStreams(treeMap,3);

        ce.getReverseViewOfKeys(treeMap);

        ce.sortTreeMapKeys();
    }

    //Write a Java program to update specific array element by given element
    public  void updateElementInArray(int position, int[] arr, int newValue){
        System.out.println("********** 1 **************");
        if(arr.length <=position || position < 0){
            System.out.println("Out of Bound");
            return ;
        }
        System.out.println("Orginal Array :");
        for(int a : arr)
            System.out.print(a+" ");
        arr[position] = newValue;
        System.out.println();
        System.out.println("Modified Array :");
        for(int a : arr)
            System.out.print(a+" ");
        System.out.println();
    }

    //Write a Java program to search an element in a array list.
    public <T> int searchElementInArray(List<Integer> arr, int searchValue){
        System.out.println("********** 2 **************");
        if(arr ==null || arr.size() ==0 ){
            System.out.println("Array is empty");
            return -1;
        }
        System.out.println("Given Array "+arr);
        for(int a=0;a<arr.size();a++)
            if(arr.get(a)==searchValue) {
                System.out.println("Element "+searchValue+" found at index "+a);
                return a;
            }
        System.out.println("Element not found in the array");
        return -1;
       // return arr.indexOf(searchValue);
        //boolean return type arr.
    }

//    Write a Java program to copy one array list into another.
    public void copyArrayList(List<Integer> orginalList, List<Integer> toCopiedList){
        System.out.println("********** 3 **************");
        if(orginalList == null|| toCopiedList == null || orginalList.size()==0) return;
        toCopiedList.addAll(orginalList);
        //toCopiedList.forEach(a->System.out.println(a));
        System.out.println("This is the copied List "+toCopiedList);
          //for(int a : orginalList)
          //   toCopiedList.add(a);
    }

//    Write a Java program to iterate a linked list in reverse order.

    public void linkedListReverse(LinkedList<Integer> list){
        System.out.println("********** 4 **************");
        if(list == null || list.size()==0) return;
        Iterator<Integer> it = list.descendingIterator();
        System.out.println("LinkedList In Reverse");
        while(it.hasNext())
            System.out.println(it.next());
    }

//    Write a Java program to insert some elements at the specified position into a linked list. 
    public void insertElementsInLinkedList(LinkedList<Integer> list, List<Integer> listToInsert){
        System.out.println("********** 5 **************");
        System.out.println("Before insert "+list);
        list.addAll(0,listToInsert);
        System.out.println("After insert "+list);

    }

//    Write a Java program to shuffle the elements in a linked list
    public void shuffleElementsInLinkedList(LinkedList<Integer> list){
        System.out.println("********** 6 **************");
        System.out.println("Before shuffling "+list);
        Collections.shuffle(list);
        System.out.println("After insert "+list);

    }
//    Write a Java program to convert a hash set to a tree set
    public <T> TreeSet<T> hashSetToTreeSet(HashSet<T> set){
        System.out.println("********** 7 **************");
        TreeSet<T> treeSet = new TreeSet<>(set);
        System.out.println("After hashset to TreeSet");
        treeSet.forEach(a->System.out.println(a));
        System.out.println(treeSet);
        return treeSet;

    }
//    Write a Java program to compare two tree sets.
    public void compareTreeSets(){
        System.out.println("********** 8 **************");
        TreeSet<Integer> treeSet1 = new TreeSet<>();

        treeSet1.add(1);
        treeSet1.add(4);
        treeSet1.add(3);
        treeSet1.add(2);
        TreeSet<Integer> treeSet2 = new TreeSet<>();

        treeSet2.add(1);
        treeSet2.add(4);
        treeSet2.add(3);
        treeSet2.add(22);

        for(int a: treeSet1)
            System.out.println(treeSet2.contains(a));

    }

//    Write a Java program to get an element in a tree set which is strictly less than the given element.
    public void getElementLTGiven(TreeSet<Integer> set, int value){
        System.out.println("********** 9 **************");
        if(set.first()<value)
            System.out.println("The value less than the given in the Set is "+ set.first());
        else
            System.out.println("All the elements are greater than given one");
    }

//    Write a Java program to convert a priority queue to an array containing all of the elements of the queue.
    public Integer[] pQToArray(PriorityQueue<Integer> pq){
        System.out.println("********** 10 **************");
        Integer[] array = new Integer[pq.size()];
        pq.toArray(array);
        System.out.println("After converting to Array");
        for(int a : array)
            System.out.print(a+" ");
        System.out.println();
        return array;


    }

//    Write a Java program to check whether a map contains key-value mappings (empty) or not.
    public void checkIFMapIsEmpty(Map<Integer, String> map){
        System.out.println("********** 11 **************");
        if(map.isEmpty())
            System.out.println("Map Is Empty");
        else{
            System.out.println("Map contains values");
            map.forEach((k,v)->System.out.println("Key :"+k+" Value : "+v));
        }
    }

//    Write a Java program to get a portion of a map whose keys are greater than to a given key.
    public void getPortionOfMap(TreeMap<Integer, String> map, int value){
        System.out.println("********** 12 **************");
        if(map.isEmpty())
            System.out.println("Map Is Empty");
        else {
           map.tailMap(value).forEach((k,v)->System.out.println("Key :"+k+" Value : "+v));
        }
    }

    //    Write a Java program to get a portion of a map whose keys are greater than to a given key.
    public void getPortionOfMapStreams(Map<Integer, String> map, int value){
        System.out.println("********** 12 **************");
        if(map.isEmpty())
            System.out.println("Map Is Empty");
        else {
            map.keySet().stream().filter(a->a>=value).forEach(a->System.out.println(a));
        }
    }

//    Write a Java program to get a reverse order view of the keys contained in a given map.
    public void getReverseViewOfKeys(TreeMap<Integer, String> map){
        System.out.println("********** 13 **************");
        System.out.println("Initial Key Set is: ");
        System.out.println(map.keySet());
        System.out.println("Reverse view of Keys is: ");
        System.out.println(map.descendingKeySet());
    }

    // Write a Java program to sort keys in Tree Map by using comparator.

    public void sortTreeMapKeys(){
        System.out.println("********** 14 **************");
        //No need to sort the TreeMap as it is already sorted in ascending order of keys
        TreeMap<String, String> map = new TreeMap<>((a,b)->b.compareTo(a));
        map.put("A","1");
        map.put("B","1");
        map.put("C","1");
        map.put("D","1");
        map.forEach((a,b)->System.out.println(a+"   "+b));


    }

}
